package com.graphaware.pizzeria.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Discount {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String discountCode;

    private int enabled;

    private String description;

    private Integer discountPercentage;

    private Integer discountValue;
}
