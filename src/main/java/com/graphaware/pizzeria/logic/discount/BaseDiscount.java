package com.graphaware.pizzeria.logic.discount;

import com.graphaware.pizzeria.model.Discount;
import com.graphaware.pizzeria.model.Pizza;
import com.graphaware.pizzeria.repository.DiscountRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * Basic implementation which allows for easy addition of generic discounts through a fixed value or percentage.
 */
@RequiredArgsConstructor
public class BaseDiscount {

    private String discountCode;
    private final DiscountRepository discountRepository;

    public boolean isEnabled(){
        return discountRepository
                .findByDiscountCode(discountCode)
                .getEnabled() == 1;
    }

    public double computeDiscount(List<Pizza> pizzas){

       Discount discount = discountRepository.findByDiscountCode(discountCode);
       Integer discountValue = discount.getDiscountValue();
       Integer discountPercentage = discount.getDiscountPercentage();

       int computedDiscount = 0;

       if(discountValue != null) computedDiscount += discountValue;

       if(discountPercentage != null){
           double totalPrice = pizzas.stream()
                   .map(Pizza::getPrice)
                   .mapToDouble(Double::doubleValue)
                   .sum();
           computedDiscount += totalPrice * discountPercentage / 100;
       }

       return computedDiscount;
    }

    public void setDiscountCode(String discountCode){ this.discountCode = discountCode; }
}
