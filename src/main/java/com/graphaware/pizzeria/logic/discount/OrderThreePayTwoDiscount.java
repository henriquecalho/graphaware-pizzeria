package com.graphaware.pizzeria.logic.discount;

import com.graphaware.pizzeria.model.Pizza;
import com.graphaware.pizzeria.repository.DiscountRepository;

import java.util.Comparator;
import java.util.List;

public class OrderThreePayTwoDiscount extends BaseDiscount {

    public OrderThreePayTwoDiscount(DiscountRepository discountRepository) {
        super(discountRepository);
        setDiscountCode("ORDER_THREE_PAY_TWO");
    }

    @Override
    public double computeDiscount(List<Pizza> pizzas) {
        if(pizzas == null || pizzas.size() < 3)
            return 0;

        return pizzas.stream()
                .min(Comparator.comparing(Pizza::getPrice))
                .orElseThrow(IllegalArgumentException::new)
                .getPrice();
    }
}
