package com.graphaware.pizzeria.repository;

import com.graphaware.pizzeria.model.Discount;
import org.springframework.data.repository.CrudRepository;

public interface DiscountRepository extends CrudRepository<Discount, Long> {

	Discount findByDiscountCode(String discountCode);
}
